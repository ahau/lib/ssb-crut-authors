const test = require('tape')
const Crut = require('../')
const { SSB, Spec } = require('./helpers')

const body = 'dinosaur notes'

function Expected (ssb, id, authors) {
  return {
    key: id,
    type: 'wiki',
    originalAuthor: ssb.id,
    body: 'dinosaur notes',
    authors, // <<
    tombstone: null,
    recps: null,
    states: [],
    conflictFields: []
  }
}

test('create (with authors.add = [FeedId])', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: { add: [alice.id] } }
  alice.wiki.create(A, (err, id) => {
    if (err) throw err

    alice.wiki.read(id, (err, record) => {
      if (err) throw err
      const expected = Expected(alice, id, {
        [alice.id]: [{ start: 0, end: null }]
      })

      t.deepEqual(record, expected, 'can read')

      alice.close()
      t.end()
    })
  })
})

test('create (with authors.add = ["*"])', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: { add: ['*'] } }
  alice.wiki.create(A, (err, id) => {
    t.error(err, 'creates record')
    alice.wiki.read(id, (err, record) => {
      if (err) t.error(err)

      const expected = Expected(alice, id, {
        '*': [{ start: record.authors['*'][0].start, end: null }]
      })

      t.deepEqual(record, expected, 'can read')

      alice.close()
      t.end()
    })
  })
})

/* unhappy cases */

test('create (no authors)', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const A = { body }
  alice.wiki.create(A, (err, id) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects no authors')
    alice.close()
    t.end()
  })
})

test('create (empty authors)', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: {} }
  alice.wiki.create(A, (err, id) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects empty authors')
    alice.close()
    t.end()
  })
})

test('create (empty authors.add)', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const A = { body, authors: { add: [] } }
  alice.wiki.create(A, (err, id) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects empty authors.add')
    alice.close()
    t.end()
  })
})

test('create (with authors.remove)', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const D = { authors: { remove: [alice.id] } }
  alice.wiki.create(D, (err, rootId) => {
    t.match(err.message, /Invalid initial authors/, '#create rejects only authors.remove')
    alice.close()
    t.end()
  })
})

test('create (with malformed authors.add)', t => {
  const alice = SSB()
  alice.wiki = new Crut(alice, Spec())

  const E = { authors: { add: ['@dog'] } }
  alice.wiki.create(E, (err, rootId) => {
    t.match(err.message, /malformed feedId/, '#create rejects only authors.remove')

    alice.close()
    t.end()
  })
})

test('create (with isValidNextStep)', t => {
  t.plan(1)

  const alice = SSB()
  alice.wiki = new Crut(alice, Spec({
    isValidNextStep (context, msg) {
      t.pass('our isValid is called')
      return true
    }
  }))

  const A = { body, authors: { add: [alice.id] } }
  alice.wiki.create(A, (err) => {
    if (err) throw err
    alice.close()
  })
})
